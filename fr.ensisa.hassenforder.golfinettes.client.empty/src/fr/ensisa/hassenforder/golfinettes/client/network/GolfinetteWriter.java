package fr.ensisa.hassenforder.golfinettes.client.network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.nio.ByteBuffer;

import fr.ensisa.hassenforder.golfinettes.client.model.Event;
import fr.ensisa.hassenforder.golfinettes.client.model.Usage;
import fr.ensisa.hassenforder.golfinettes.client.model.Usage.UsageState;
import fr.ensisa.hassenforder.golfinettes.network.Protocol;
import fr.ensisa.hassenforder.network.BasicAbstractWriter;

public class GolfinetteWriter extends BasicAbstractWriter {
	
	private String host;
	private int port;
	
	public GolfinetteWriter(String host, int port) {
        super(null);
        this.host = host;
        this.port = port;
    }

	private void writeAsByte(int value) {
        writeByte((byte) (value & 0xFF));
	}
	
	

	@Override
	public void send() {
        byte[] message = baos.toByteArray();
    	DatagramSocket socket = null;
        try {
        	InetAddress target = InetAddress.getByName(host);
        	DatagramPacket packet = new DatagramPacket(message, message.length, target, port);
        	System.out.println("SIGFOX packet sent with : "+(message.length)+" bytes all inclusive");
        	System.out.println("SIGFOX packet sent with : "+(message.length-20)+" bytes without type, id and timestamp");
        	socket = new DatagramSocket();
        	socket.send(packet);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
			if (socket != null) socket.close();
		}
	}

	public void createSigFoxMove(Event lastEvent) {
		writeAsByte(Protocol.SIGFOX_MOVE);
		writeLong(lastEvent.getId());
		writeLong(lastEvent.getTimestamp().getTime());
		writeFloat24(lastEvent.getLocation().getLatitude());
		writeFloat24(lastEvent.getLocation().getLongitude());
	}

	public void createSigFoxAlarm(Event lastEvent) {
		writeAsByte(Protocol.SIGFOX_ALARM);
		writeLong(lastEvent.getId());
		writeLong(lastEvent.getTimestamp().getTime());
		writeFloat24(lastEvent.getLocation().getLatitude());
		writeFloat24(lastEvent.getLocation().getLongitude());
	}

	public void createSigFoxBorrow(Event lastEvent) {
		writeAsByte(Protocol.SIGFOX_BORROW);
		writeLong(lastEvent.getId());
		writeLong(lastEvent.getTimestamp().getTime());
		writeFloat24(lastEvent.getLocation().getLatitude());
		writeFloat24(lastEvent.getLocation().getLongitude());
	}

	public void createSigFoxClimat(Event lastEvent) {
		writeAsByte(Protocol.SIGFOX_CLIMAT);
		writeLong(lastEvent.getId());
		writeLong(lastEvent.getTimestamp().getTime());
		writeFloat24(lastEvent.getLocation().getLatitude());
		writeFloat24(lastEvent.getLocation().getLongitude());
		writeAsByte(byteFrom2Bytes(lastEvent.getLocation().getTemperature(),lastEvent.getBattery().getMode().ordinal(),5));
		writeAsByte(lastEvent.getUsage().getUsage().ordinal());
		writeAsByte(lastEvent.getBattery().getLoad());
		writeAsByte(byteFrom2Bytes(lastEvent.getLocation().getHumidity(),lastEvent.getUsage().getUsage().ordinal(),5));
	}

	public void writeFloat24(float v) {
//		byte[] tab = new byte[3];
//		tab[2]=(byte) ((Float.floatToIntBits(v) >> 16) & 0xFF);
//		System.out.println(Float.floatToIntBits(v) >> 16);
//		tab[1]=(byte) ((Float.floatToIntBits(v) >> 8) & 0xFF);
//		tab[0]=(byte) ((Float.floatToIntBits(v) >> 0) & 0xFF);
		byte[] tab = ByteBuffer.allocate(4).putFloat(v).array();
		writeByte(tab[0]);
		writeByte(tab[1]);
		writeByte(tab[2]);
	}
	
	//Il faut que left s'ecrive sur 7 bits au plus
	public static int byteFrom2Bytes(int left,int right,int sizeLeft) {
		int sizeRight=8-sizeLeft;
		left*=Math.pow(2,sizeRight);
		return  (left+right);
		
		
	}
	
	public static byte[] TwoBytesFrom1Byte(int integer,int sizeLeft) {
		byte[] ans=new byte[2];
		int right=integer;
		for(int i = 7; i >= 8-sizeLeft;i--) {
			if(right>=Math.pow(2, i)) {
				right-=Math.pow(2,i);
			}
		}
		ans[1]=(byte) right;
		ans[0]=(byte) ((integer-right)/Math.pow(2, 8-sizeLeft));
		return ans;
		
	}
	
	private static float scaling(float min, float max, float value_to_scale, int nb_values) {
		float h = (max-min)/nb_values;
		float ans = value_to_scale-value_to_scale%h;
		if(value_to_scale-ans >= h/2) {
			ans+=h;
		}
		return ans;
	}
}
