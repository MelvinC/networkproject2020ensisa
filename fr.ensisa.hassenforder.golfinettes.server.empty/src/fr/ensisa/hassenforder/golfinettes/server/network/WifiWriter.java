package fr.ensisa.hassenforder.golfinettes.server.network;

import java.io.OutputStream;
import java.util.Collection;
import java.util.List;

import fr.ensisa.hassenforder.golfinettes.network.Protocol;
import fr.ensisa.hassenforder.golfinettes.server.model.Event;
import fr.ensisa.hassenforder.golfinettes.server.model.Golfinette;
import fr.ensisa.hassenforder.golfinettes.server.model.Version;
import fr.ensisa.hassenforder.network.BasicAbstractWriter;

public class WifiWriter extends BasicAbstractWriter {

	public WifiWriter(OutputStream outputStream) {
		
		super (outputStream);
		
	}

	public void createWifiMove(Event lastEvent) {
		String event = lastEvent.getKind() + " | " + new Long(lastEvent.getId()).toString() + " | " + lastEvent.getTimestamp().toString() + " | " + lastEvent.getLocation().toString();
		writeString(event);
	}
	
	public void createWifiAlarm(Event lastEvent) {
		writeInt(Protocol.SIGFOX_ALARM);
		writeLong(lastEvent.getId());
		writeLong(lastEvent.getTimestamp().getTime());
		writeFloat(lastEvent.getLocation().getLatitude());
		writeFloat(lastEvent.getLocation().getLongitude());
	}

	public void createWifiBorrow(Event lastEvent) {
		writeInt(Protocol.SIGFOX_BORROW);
		writeLong(lastEvent.getId());
		writeLong(lastEvent.getTimestamp().getTime());
		writeFloat(lastEvent.getLocation().getLatitude());
		writeFloat(lastEvent.getLocation().getLongitude());
	}

	public void createWifiClimat(Event lastEvent) {
		writeInt(Protocol.SIGFOX_CLIMAT);
		writeLong(lastEvent.getId());
		writeLong(lastEvent.getTimestamp().getTime());
		writeFloat(lastEvent.getLocation().getLatitude());
		writeFloat(lastEvent.getLocation().getLongitude());
	}
}
