package fr.ensisa.hassenforder.golfinettes.server.network;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Date;

import fr.ensisa.hassenforder.golfinettes.network.Protocol;
import fr.ensisa.hassenforder.golfinettes.server.model.Battery;
import fr.ensisa.hassenforder.golfinettes.server.model.Battery.BatteryMode;
import fr.ensisa.hassenforder.golfinettes.server.model.Event;
import fr.ensisa.hassenforder.golfinettes.server.model.Location;
import fr.ensisa.hassenforder.golfinettes.server.model.Usage;
import fr.ensisa.hassenforder.golfinettes.server.model.Usage.BorrowerEvent;
import fr.ensisa.hassenforder.golfinettes.server.model.Usage.UsageState;
import fr.ensisa.hassenforder.network.BasicAbstractReader;

public class GolfinetteReader extends BasicAbstractReader {

	private Event event;
	
	public GolfinetteReader(byte [] data) {
		super (new ByteArrayInputStream(data));
	}

	private int readAsByte() {
        return (int) (readByte() & 0xFF);
	}

	public Event readSigFoxMove () throws IOException {
		event = new Event(readLong(), readLong(), "SigFoxMove");
		event.withLocation(new Location(readFloat24(), readFloat24(), 0, 0));
		System.out.println(event.getLocation());
		return event;
	}
	
	public Event readSigFoxBorrow () throws IOException {
		event = new Event(readLong(), readLong(), "SigFoxBorrow");
		event.withLocation(new Location(readFloat24(), readFloat24(), 0, 0));
		System.out.println(event.getLocation());
		return event;
	}
	
	public Event readSigFoxAlarm () throws IOException {
		event = new Event(readLong(), readLong(), "SigFoxAlarm");
		event.withLocation(new Location(readFloat24(), readFloat24(), 0, 0));
		System.out.println(event.getLocation());
		return event;
	}
	
	public Event readSigFoxClimat () throws IOException {
		event = new Event(readLong(), readLong(), "SigFoxClimat");
		event.withLocation(new Location(readFloat24(), readFloat24(), 0, 0));
		System.out.println(event.getLocation());
		return event;
	}

	public void receive() throws IOException {
		type = readInt ();
		switch (type) {
		case 0 : break;
		case Protocol.SIGFOX_MOVE: 		event = readSigFoxMove(); break;
		case Protocol.SIGFOX_BORROW: 	event = readSigFoxBorrow(); break;
		case Protocol.SIGFOX_ALARM: 	event = readSigFoxAlarm(); break;
		case Protocol.SIGFOX_CLIMAT: 	event = readSigFoxClimat(); break;
		}
	}

	public Event getEvent() {
		return event;
	}
	
	public float readFloat24() {
		byte[] tab = new byte[4];
		tab[0] = readByte();
		tab[1] = readByte();
		tab[2] = readByte();
		tab[3] = (byte) 0;
//		int asInt = (tab[1] & 0xFF) 
//	            | ((tab[2] & 0xFF) << 8) 
//	            | ((tab[3] & 0xFF) << 16) 
//	            | ((tab[0] & 0xFF) << 24);
//		float asFloat = Float.intBitsToFloat(asInt);
//		return asFloat;
		return ByteBuffer.wrap(tab).getFloat();		
	}
}
