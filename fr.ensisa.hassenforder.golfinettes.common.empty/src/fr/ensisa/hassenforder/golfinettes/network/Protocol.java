package fr.ensisa.hassenforder.golfinettes.network;

public class Protocol {

    public static final int GOLFINETTES_SIGFOX_PORT = 6666;
    public static final int GOLFINETTES_WIFI_PORT	= 7777;

    // left to help you (or not)
	public final static int SIGFOX_MOVE = 0x01;
	public final static int SIGFOX_ALARM = 0x02;
	public final static int SIGFOX_BORROW = 0x03;
	public final static int SIGFOX_CLIMAT = 0x04;
}
