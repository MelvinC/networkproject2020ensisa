package fr.ensisa.hassenforder.golfinettes.admin.network;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import fr.ensisa.hassenforder.golfinettes.admin.model.Event;
import fr.ensisa.hassenforder.golfinettes.admin.model.Golfinette;
import fr.ensisa.hassenforder.golfinettes.network.Protocol;
import fr.ensisa.hassenforder.network.BasicAbstractReader;

public class AdminReader extends BasicAbstractReader {

    private String version;
    private List<Event> events;
    private List<Golfinette> golfinettes;

    public AdminReader(InputStream inputStream) {
        super(inputStream);
    }

    public void receive() throws IOException {
        type = readInt();
        version = null;
		switch (type) {
			case 0 : break;
			case Protocol.SIGFOX_MOVE: 		events.add(readMove()); break;
			case Protocol.SIGFOX_BORROW: 	events.add(readBorrow()); break;
			case Protocol.SIGFOX_ALARM: 	events.add(readAlarm()); break;
			case Protocol.SIGFOX_CLIMAT: 	events.add(readClimat()); break;
		}
        golfinettes = null;
    }
    
    public Event readMove () throws IOException {
    	return null;
	}
	
	public Event readBorrow () throws IOException {
		return null;
	}
	
	public Event readAlarm () throws IOException {
		return null;
	}
	
	public Event readClimat () throws IOException {
		return null;
	}

	public String getVersion() {
		return version;
	}

	public List<Event> getEvents() {
		return events;
	}

	public List<Golfinette> getGolfinettes() {
		return golfinettes;
	}

}
